﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckGameOver : MonoBehaviour
{
    BoxCollider box;
    int checkColl;

    public GameObject gameOverPannel;

    void Awake()
    {
        box = GetComponent<BoxCollider>();
        box.enabled = false;
        StartCoroutine(ShowBoxColl());
    }

    IEnumerator ShowBoxColl()
    {
        yield return new WaitForSeconds(9);
        box.enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "AI Red" || other.gameObject.name == "AI Blue" || other.gameObject.name == "AI Green")
        {
            checkColl++;
            if(checkColl >= 2)
            {
                gameOverPannel.SetActive(true);
            }
        }
    }
}
