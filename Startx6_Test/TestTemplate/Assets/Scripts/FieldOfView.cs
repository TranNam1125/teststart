﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
    public float viewRadius;
    public float viewDieRadius;
    [Range(0, 360)]
    public float viewAngle;
    public LayerMask targetMask;
    public LayerMask obstaglesMask;
    public LayerMask dieMask;
    public Transform visibleTarget;
    public Transform dieTarget;

    private void Start()
    {
        StartCoroutine(FindTargetWithDelay(5));
        StartCoroutine(FindDieWithDelay(0.1f));
    }

    IEnumerator FindTargetWithDelay(float delay)
    {
        while (true)
        {
            FindDieTarget();
            yield return new WaitForSeconds(delay);
            FindVisibleTarget();
        }
    }

    IEnumerator FindDieWithDelay(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindDieTarget();
        }
    }

    void FindVisibleTarget()
    {
        Collider[] targetInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, targetMask);
        for (int i = 0; i < targetInViewRadius.Length; i++)
        {
            Transform target = targetInViewRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            if(Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
            {
                float dstToTarget = Vector3.Distance(transform.position, target.position);
                if(!Physics.Raycast(transform.position, dirToTarget, dstToTarget, obstaglesMask))
                {
                    visibleTarget = target;
                }
            }
        }
    }

    void FindDieTarget()
    {
        Collider[] targetDieRadius = Physics.OverlapSphere(transform.position, viewDieRadius, dieMask);
        for (int i = 0; i < targetDieRadius.Length; i++)
        {
            Transform target = targetDieRadius[i].transform;
            Vector3 dirToTarget = (target.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
            {
                float dstToTarget = Vector3.Distance(transform.position, target.position);
                if (!Physics.Raycast(transform.position, dirToTarget, dstToTarget, obstaglesMask))
                {
                    dieTarget = target;
                }
            }
        }
    }

    public Vector3 DirFromAngle(float angleInDeGrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDeGrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleInDeGrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDeGrees * Mathf.Deg2Rad));
    }
}
