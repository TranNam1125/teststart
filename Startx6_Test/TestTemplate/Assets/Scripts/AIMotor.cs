﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class AIMotor : MonoBehaviour
{
    FieldOfView field;
    ButtonFollow buttonFollow;
    public Transform[] point;
    public Animator anim;
    NavMeshAgent agent;
    public float allowPlayerRotation = 0.1f;
    public bool isR, isB, isG;
    Vector3 pointStart;
    int pointRandom;
    bool isDead = false;
    public GameObject buttonShow;
    public Transform playerPos;
    public GameObject winPannel;

    public Text txt;
    bool checkShowTime = false;
    public GameObject gameOverPannel;

    [Header("Animation Smoothing")]
    [Range(0, 1f)]
    [HideInInspector]
    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    [HideInInspector]
    public float StopAnimTime = 0.15f;

    void Start()
    {
        pointStart = transform.position;
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        field = GetComponent<FieldOfView>();
        if (isR)
            StartCoroutine(MoveToPoint(0.1f, 0));
        else if (isB)
            StartCoroutine(MoveToPoint(0.1f, 1));
        else if (isG)
        {
            pointRandom = Random.Range(0, 2);
            StartCoroutine(MoveToPoint(0.1f, 2));
        }
        CheckAnim(3);
    }

    void Update()
    {
        if (isDead)
        {
            agent.enabled = false;
            anim.enabled = false;
            if (isR)
                gameObject.layer = 12;
            else if (isB)
                gameObject.layer = 13;
            else if (isG)
                gameObject.layer = 14;

        }
        //TargetFocus();

        if (Vector3.Distance(transform.position, playerPos.position) <= 1.5f && !isDead)
            ShowButton(true);
        else
            ShowButton(false);

        if (field.dieTarget != null)
        {
            gameOverPannel.SetActive(true);
        }

    }

    IEnumerator MoveToPoint(float timeDelay, int _point)
    {
        while (timeDelay > 0)
        {
            txt.text = timeDelay.ToString();
            yield return new WaitForSeconds(1);
            timeDelay--;
        }
        txt.text = "";
        yield return new WaitForSeconds(0.1f);
        if (isDead)
            yield break;
        agent.SetDestination(point[_point].position);
        CheckAnim(3);
    }

    private void OnTriggerEnter(Collider other)
    {
        CheckAnim(0);
        if (other.gameObject.name == "PRed")
        {
            if (isR)
            {
                StartCoroutine(MoveToPoint(5, 1));
            }
            else if (isB)
                StartCoroutine(MoveToPoint(5, 2));
            else if (isG)
            {
                if(pointRandom == 0)
                {
                    StartCoroutine(MoveToPoint(5, 1));
                }else if(pointRandom == 1)
                {
                    StartCoroutine(GoBack(5));
                }
            }
        }
        else if (other.gameObject.name == "PBlue")
        {
            if (isR)
                StartCoroutine(MoveToPoint(5, 2));
            else if (isB)
                StartCoroutine(MoveToPoint(5, 0));
            else if (isG)
            {
                if (pointRandom == 0)
                {
                    StartCoroutine(GoBack(5));
                }
                else if (pointRandom == 1)
                {
                    StartCoroutine(MoveToPoint(5, 0));
                }
            }
        }
        else if (other.gameObject.name == "PGreen")
        {
            if (isR || isB)
                StartCoroutine(GoBack(10));
            else if(isG)
                StartCoroutine(MoveToPoint(10, pointRandom));
        }
    }

    IEnumerator GoBack(float timeDelay)
    {
        while (timeDelay > 0)
        {
            txt.text = timeDelay.ToString();
            yield return new WaitForSeconds(1);
            timeDelay--;
        }
        txt.text = "";
        yield return new WaitForSeconds(0.1f);
        if (isDead)
            yield break;
        agent.SetDestination(pointStart);
    }

    public void CheckAnim(float Speed)
    {
        if (Speed > allowPlayerRotation)
        {
            anim.SetFloat("Blend", Speed, StartAnimTime, Time.deltaTime);
        }
        else if (Speed < allowPlayerRotation)
        {
            anim.SetFloat("Blend", Speed, StopAnimTime, Time.deltaTime);
        }
    }

    public void ButtonCheck()
    {
        field.enabled = false;
        KillEnemy.instance.enemyKill++;
        if(KillEnemy.instance.enemyKill >= 2)
        {
            winPannel.SetActive(true);
        }
        isDead = true;
    }

    void TargetFocus()
    {
        if (field.visibleTarget != null && !isDead)
        {
            agent.SetDestination(field.visibleTarget.position);
        }
    }

    public void ShowButton(bool show)
    {
        buttonShow.SetActive(show);
    }
}
