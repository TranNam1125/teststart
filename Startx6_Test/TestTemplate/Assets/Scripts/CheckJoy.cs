﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckJoy : MonoBehaviour
{

    public Image joys;
    void Start()
    {

        joys = GetComponent<Image>();

        joys.color = new Color(joys.color.r, joys.color.g, joys.color.b, 0f);
    }
    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            joys.color = new Color(joys.color.r, joys.color.g, joys.color.b, 0.3f);
        }
        else
        {
            joys.color = new Color(joys.color.r, joys.color.g, joys.color.b, 0f);
        }
    }


}
